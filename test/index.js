var assert = require('assert');
var custerror = require('../lib').default;
function tryParseJSON(string) {
	try {
		return JSON.parse(string);
	} catch(err) {
		return false;
	}

}
/*global describe it*/
describe('custerror()', function() {
	var tests = [{
		errName : 'CustomError1',
		message : 'String message'
	}, {
		errName : 'CustomError2',
		message : {
			value : 'JSON message'
		}
	}];
	tests.forEach(test => {
		it(`should create custom error type named as ${test.errName}`, function() {
			try {
				let CustomError = custerror(test.errName);
				throw new CustomError(test.message);
			} catch(err) {
				assert.equal(err.name, test.errName);
				assert.deepEqual(tryParseJSON(err.message) || err.message, test.message);
			}
		});
	});
});
