import _ from 'lodash';

export default function custerror(errName, otherAttrs) {
	return class extends Error {
		constructor(message) {
			if (_.isPlainObject(message))
				message = JSON.stringify(message, {}, 2);
			super(message);
			this.name = errName;
			if (typeof Error.captureStackTrace === 'function') {
				Error.captureStackTrace(this);
			}
			else {
				this.stack = (new Error(message)).stack;
			}
			Object.assign(this, otherAttrs
				? _.isPlainObject(otherAttrs)
					? otherAttrs
					: { otherAttrs }
				: {}
			);
		}
	};
}
